/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DimensionInfo {

    private String firstDimension;
    private String secondDimension;
    private String firstDimensionCode;
    private String secondDimensionCode;
    private String firstDimensionName;
    private String secondDimensionName;

    @JsonProperty(CefNames.FIRST_DIMENSION)
    public String getFirstDimension() {
        return firstDimension;
    }

    public void setFirstDimension(String firstDimension) {
        this.firstDimension = firstDimension;
    }

    @JsonProperty(CefNames.SECOND_DIMENSION)
    public String getSecondDimension() {
        return secondDimension;
    }

    public void setSecondDimension(String secondDimension) {
        this.secondDimension = secondDimension;
    }

    @JsonProperty(CefNames.FIRST_DIMENSION_CODE)
    public String getFirstDimensionCode() {
        return firstDimensionCode;
    }

    public void setFirstDimensionCode(String firstDimensionCode) {
        this.firstDimensionCode = firstDimensionCode;
    }

    @JsonProperty(CefNames.SECOND_DIMENSION_CODE)
    public String getSecondDimensionCode() {
        return secondDimensionCode;
    }

    public void setSecondDimensionCode(String secondDimensionCode) {
        this.secondDimensionCode = secondDimensionCode;
    }

    @JsonProperty(CefNames.FIRST_DIMENSION_NAME)
    public String getFirstDimensionName() {
        return firstDimensionName;
    }

    public void setFirstDimensionName(String firstDimensionName) {
        this.firstDimensionName = firstDimensionName;
    }

    @JsonProperty(CefNames.SECOND_DIMENSION_NAME)
    public String getSecondDimensionName() {
        return secondDimensionName;
    }

    public void setSecondDimensionName(String secondDimensionName) {
        this.secondDimensionName = secondDimensionName;
    }
}
