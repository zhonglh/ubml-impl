/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.openatom.ubml.model.common.definition.cef.entity.DynamicPropSetInfo;
import org.openatom.ubml.model.common.definition.cef.entity.MdRefInfo;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Serializer Of DynamicPropSetInfo
 *
 * @ClassName: DynamicPropSetInfoDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DynamicPropSetInfoDeserializer extends JsonDeserializer<DynamicPropSetInfo> {
    @Override
    public DynamicPropSetInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        DynamicPropSetInfo dynamicPropSetInfo = new DynamicPropSetInfo();

        if (SerializerUtils.readNullObject(jsonParser)) {
            return dynamicPropSetInfo;
        }
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(dynamicPropSetInfo, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return dynamicPropSetInfo;
    }

    private void readPropertyValue(DynamicPropSetInfo dynamicPropSetInfo, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.DYNAMIC_PROP_REPOSITORY_COMP:
                dynamicPropSetInfo.setDynamicPropRepositoryComp(readMdRefInfo(jsonParser));
                break;
            case CefNames.DYNAMIC_PROP_SERIALIZER_COMP:
                dynamicPropSetInfo.setDynamicPropSerializerComp(readMdRefInfo(jsonParser));
                break;
            default:
                throw new RuntimeException(String.format("MappingInfoDeserializer未识别的属性名：%1$s", propName));
        }
    }

    private MdRefInfo readMdRefInfo(JsonParser jsonParser) {
        MdRefInfo mdRefInfo = new MdRefInfo();
        if (SerializerUtils.readNullObject(jsonParser)) {
            return mdRefInfo;
        }

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readMdRefInfoPropertyValue(mdRefInfo, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return mdRefInfo;
    }

    private void readMdRefInfoPropertyValue(MdRefInfo mdRefInfo, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.PKG_NAME:
                mdRefInfo.setPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.ID1:
            case CefNames.ID:
                mdRefInfo.setId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.NAME:
                mdRefInfo.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                throw new RuntimeException(String.format("MappingInfoDeserializer未识别的属性名：%1$s", propName));
        }
    }
}
