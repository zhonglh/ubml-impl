/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.manager;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.MavenUtils;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.core.MavenUtilsCore;
import java.io.File;
import java.util.List;

public class MavenDeploymentForJarManager extends MavenDeploymentManager {
    private MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();

    private FileServiceImp fileServiceImp = new FileServiceImp();

    private PomManager pomManager = new PomManager();

    private String projPath;
    private String repoId;
    private String dependencyVersion;
    private String mavenPath;

    public MavenDeploymentForJarManager() {

    }

    public void deploy(String projPath, String repoId, String dependencyVersion, String mavenPath) {
        this.projPath = projPath;
        this.repoId = repoId;
        this.dependencyVersion = dependencyVersion;
        this.mavenPath = mavenPath;
        beforeDeploy();

        doDeploy();

        afterDeploy();
    }

    protected void beforeDeploy() {
        boolean isModelExist = isJavaProjExist(projPath);
        if (!isModelExist) {
            throw new RuntimeException("未找到Java模板工程目录，请先生成Java代码");
        }

        setCodeRepoUrl(projPath, repoId);
    }

    protected void doDeploy() {
        // 推送api
        String codePath = projPath + File.separator + "java" + File.separator + Utils.getMavenProName();
        boolean isApiPushed = mavenUtilsCore.exeMavenCommand(codePath, mavenPath, "deploy -pl api -am");
        if (!isApiPushed) {
            throw new RuntimeException("部署api失败");
        }
    }

    protected void afterDeploy() {

    }

    private boolean isJavaProjExist(String projPath) {
        //判断模板工程是否已创建。
        if (fileServiceImp.getProjectPath(projPath) != null) {
            String subpropath = fileServiceImp.getProjectPath(projPath);
            String fullPath = fileServiceImp.getCombinePath(projPath, subpropath);
            List<File> list = fileServiceImp.getAllFiles(fullPath);
            for (File file : list) {
                if (file.toString().endsWith("pom.xml")) {
                    return true;
                }
            }
        }
        return false;
    }

    private void setCodeRepoUrl(String projPath, String repoId) {
        String codePomPath = projPath + File.separator + "java" + File.separator + Utils.getMavenProName() + File.separator + MavenUtils.POM_FILE;
        pomManager.setRepoUrl(codePomPath, repoId);
    }

}
