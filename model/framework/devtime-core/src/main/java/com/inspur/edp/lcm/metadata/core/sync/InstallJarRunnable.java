/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.sync;

import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenPackageRefs;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.core.MavenUtilsCore;
import com.inspur.edp.lcm.metadata.core.entity.PackageType;
import com.inspur.edp.lcm.metadata.core.manager.MavenCommandGenerator;
import com.inspur.edp.lcm.metadata.core.manager.PomManager;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InstallJarRunnable implements Runnable {
    protected String pomPath;
    protected String pomDirPath;
    protected String mavenPath;
    protected String downloadPath;
    protected boolean forceUpdateFlag;
    protected final FileServiceImp fileServiceImp = new FileServiceImp();
    protected final MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();
    private List<String> downloadMdpkgPaths;
    private String featureInName = "-api-";

    @Override
    public void run() {
        try {
            List<MavenPackageRefs> refs = getMavenPackageRefs();
            final PomManager pomManager = new PomManager();
            pomManager.updatePom(pomPath, refs, PackageType.jar.toString());
            final MavenCommandGenerator mavenCommandGenerator = new MavenCommandGenerator();
            mavenCommandGenerator.setOutputDirectory(downloadPath);
            mavenCommandGenerator.setIncludeTypes(PackageType.jar.toString());
            mavenCommandGenerator.setForceUpdate(forceUpdateFlag);
            mavenCommandGenerator.setExcludeTransitive(true);
            final String mavenCommand = mavenCommandGenerator.generateCopydependencies();
            mavenUtilsCore.exeMavenCommand(pomDirPath, mavenPath, mavenCommand);
        } finally {
            try {
                //删除pom.xml
                if (fileServiceImp.isFileExist(pomPath)) {
                    fileServiceImp.fileDelete(pomPath);
                }
                //删除\projects\maven/temp
                if (fileServiceImp.isDirectoryExist(downloadPath)) {
                    fileServiceImp.deleteAllFilesUnderDirectory(downloadPath);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private List<MavenPackageRefs> getMavenPackageRefs() {
        List<MavenPackageRefs> refs = new ArrayList<>();
        downloadMdpkgPaths.forEach(path -> {
            if (!path.contains("SNAPSHOT")) {
                return;
            }
            String dirName = new File(new File(path).getParent()).getName();
            MavenPackageRefs mavenPackageRefs = new MavenPackageRefs();
            String groupId = dirName.substring(0, dirName.indexOf("-"));
            String version = dirName.substring(dirName.lastIndexOf(featureInName) + featureInName.length() + 1);
            String artifactId = dirName.substring(dirName.indexOf("-") + 1, dirName.lastIndexOf(featureInName) + featureInName.length() - 1);
            mavenPackageRefs.setGroupId(groupId);
            mavenPackageRefs.setArtifactId(artifactId);
            mavenPackageRefs.setVersion(version);
            refs.add(mavenPackageRefs);
        });
        return refs;
    }

    public void setPomPath(String pomPath) {
        this.pomPath = pomPath;
    }

    public void setForceUpdateFlag(boolean forceUpdateFlag) {
        this.forceUpdateFlag = forceUpdateFlag;
    }

    public void setPomDirPath(String pomDirPath) {
        this.pomDirPath = pomDirPath;
    }

    public void setMavenPath(String mavenPath) {
        this.mavenPath = mavenPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public void setDownloadMdpkgPaths(List<String> downloadMdpkgPaths) {
        this.downloadMdpkgPaths = downloadMdpkgPaths;
    }
}
