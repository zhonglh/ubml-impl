/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

import com.inspur.edp.lcm.metadata.api.entity.DbConnectionInfo;
import java.io.IOException;
import java.util.List;

/**
 * @author zhaoleitr
 */
public interface ProjectExtendService {
    /**
     * 提取
     *
     * @param path
     */
    void extract(String path);

    /**
     * 部署
     *
     * @param dBConnectionInfo 数据库信息
     * @param serverPath
     * @param projPath         工程路径
     * @param restart          是否重启
     * @return
     */
    void deploy(DbConnectionInfo dBConnectionInfo, String serverPath, String projPath, String restart);

    /**
     * 文件迁移
     *
     * @param path 工程路径
     */
    void migration(String path);

    /**
     * 获取部署状态
     *
     * @param sign 加密的文件路径
     * @return 部署状态
     * @throws IOException
     */
    String getDeployStatus(String sign) throws IOException;

    List<String> batchCompile(String path, String exts, String disabledExts);
}

