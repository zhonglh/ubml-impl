/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.spi.MetadataI18nService;

public class I18nManagerHelper extends MetadataConfigurationLoader {
    private static I18nManagerHelper instance;

    private I18nManagerHelper() {
    }

    public static I18nManagerHelper getInstance() {
        if (instance == null)
            instance = new I18nManagerHelper();
        return instance;
    }

    public MetadataI18nService getI18nManager(String typeCode) {
        MetadataI18nService service = null;
        //todo 具体的获取i18nManager的具体逻辑
        MetadataI18nService manager = null;
        MetadataConfiguration data = getMetadataConfigurationData(typeCode);
        if (data != null && data.getI18nConfigData() != null) {
            Class<?> cls = null;
            try {
                if (data.getI18nConfigData() != null) {
                    cls = Class.forName(data.getI18nConfigData().getName());
                    service = (MetadataI18nService) cls.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return service;
    }
}
