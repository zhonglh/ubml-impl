/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.common.context.RuntimeContext;

public class RtCacheHandler {

    public static final String ALL_METADATA_INFO_CACHE_KEY = "allMetadata";
    public static final String ALL_PACKAGES_CACHE_KEY = "allPackages";

    public static void updateMetadataPackageCache(String packageKey, MetadataPackage packageRenamed) {
        if (packageRenamed.getHeader() != null) {
            MetadataCacheManager.removeMetadataPackageInfo(packageKey);
            MetadataCacheManager.addMetadataPackageInfo(packageKey, packageRenamed);
        }
        if (packageRenamed.getMetadataList() != null) {
            MetadataCacheManager.removeMDManifestInfo(packageKey);
            MetadataCacheManager.addMDManifestInfo(packageKey, packageRenamed.getMetadataList());
        }
        if (packageRenamed.getReference() != null) {
            MetadataCacheManager.removeMPDependence(packageKey);
            MetadataCacheManager.addMPDependence(packageKey, packageRenamed.getReference());
        }
    }

    public static void updateMetadataCache(String metadataKey, GspMetadata metadata) {
        MetadataCacheManager.removeMetadataInfo(metadataKey);
        MetadataCacheManager.addMetadataInfo(metadataKey, metadata);
        if (metadata.getRefs() != null) {
            MetadataCacheManager.removeMDDependence(metadataKey);
            MetadataCacheManager.addMDDependence(metadataKey, metadata.getRefs());
        }
    }

    public static String getMetadataCacheKey(String metadataID, String mpInfoKey) {
        return mpInfoKey + metadataID;
    }

    private static String getTenantCacheKey(String key) {
        String tenantID = RuntimeContext.getTenantID();
        return tenantID + key;
    }
}
