/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.maven.settings.Settings;
import org.apache.maven.settings.io.xpp3.SettingsXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 * Classname MavenSettingsSerializerHelper Description maven settings.xml加载器 Date 2020/1/6 19:12
 *
 * @author zhongchq
 * @version 1.0
 */
public class MavenSettingsLoader {

    private static String absoluteSettingsPath;
    private static String settingsPath = "/.m2/settings.xml";

    public static Settings getSettings() throws IOException, XmlPullParserException {
        FileServiceImp fileService = new FileServiceImp();
        //可兼容linux
        String userHome = System.getProperty("user.home");
        if (fileService.isFileExist(userHome + settingsPath)) {
            absoluteSettingsPath = userHome + settingsPath;
        }
        if (absoluteSettingsPath != null && fileService.isFileExist(absoluteSettingsPath)) {
            SettingsXpp3Reader settingsXpp3Reader = new SettingsXpp3Reader();
            return settingsXpp3Reader.read(new FileInputStream(absoluteSettingsPath));
        }
        return null;
    }

/*    String localRepository;
    MavenSettingsLoader(){
        String os = System.getProperty("user.home");
        if (FileUtils.isFileExist(os + settingsPath)) {
            absoluteSettingsPath = os + settingsPath;
        }
    }*/
}
